<div class="blog-post">

    <article class="container-fluid">

        <h2><a href="<?php $link = wp_make_link_relative(get_permalink(get_the_id()));echo urldecode($link); ?>"><?php the_title(); ?></a></h2>

        <detail class="col-xs-12 col-sm-6 col-md-6"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . '<span>  </span> ' . __('ago', 'stackdesign') . '<span> | </span>  ' . getPostViews(get_the_ID()) . ' ' . __('is viewed', 'stackdesign'); ?></detail>
        <br>
        <figure><?php if (has_post_thumbnail()) : ?><?php the_post_thumbnail('custom-thumbnail'); ?><?php endif; ?></figure>

        <summary class="row readmore"><?php the_content(); ?></summary>
        <br>
        <hr>
    </article>
</div>
<!-- /.blog-post -->