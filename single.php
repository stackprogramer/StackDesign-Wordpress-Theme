<?php
/**
 * The template for displaying all single posts and attachments
 */
get_header(); ?>

<div class="row">
    <div class="col-sm-8 blog-main">
        <main id="main" class="site-main" role="main">
			<?php
			// Start the loop.
			while (have_posts()) :
				the_post();

				/*
				 * Include the post format-specific template for the content. If you want to
				 * use this in a child theme, then include a file called content-___.php
				 * (where ___ is the post format) and that will be used instead.
				 */
				get_template_part('content', get_post_format());


				echo "<p class='postmetadata'>";
				_ex('Posted on', 'Used before publish date.', 'stackdesign');
				echo "<i class='glyphicon glyphicon-book'></i>";
				the_category(', ');
				echo "<strong>|</strong>";
				edit_post_link(__('Edit', 'stackdesign'), ' <i class="glyphicon glyphicon-edit">      </i>', '<strong>|</strong>');
				echo " <i class='glyphicon glyphicon-tags'></i>";
				the_tags(_x('Tags', 'Used before tag names.', 'stackdesign'), ' , ');
				echo "</p>";


				// If comments are open or we have at least one comment, load up the comment template.
				if (comments_open() || get_comments_number()) :
					comments_template();

				endif;


				// Previous/next post navigation.
				the_post_navigation(
					array(
						'next_text' => '<span class="meta-nav" aria-hidden="true">' . __('Next', 'twentyfifteen') . '</span> ' .
							'<span class="screen-reader-text">' . __('Next post:', 'twentyfifteen') . '</span> ' .
							'<span class="post-title">%title</span>',
						'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __('Previous', 'twentyfifteen') . '</span> ' .
							'<span class="screen-reader-text">' . __('Previous post:', 'twentyfifteen') . '</span> ' .
							'<span class="post-title">%title</span>',
					)
				);
				setPostViews(get_the_ID());
				// End the loop.
			endwhile;
			?>
        </main>
        <!-- .site-main -->

    </div>
    <!-- /.blog-main -->

	<?php get_sidebar(); ?>
</div>
<!-- /.row -->
<br>
<br>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
