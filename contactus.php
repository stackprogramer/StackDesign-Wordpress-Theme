<?php
/*
Template Name: Contact us
*/
?>

<?php get_header(); ?>


<?php
if (!empty($_POST['msg_name']) && !empty($_POST['msg_email']) && !empty($_POST['msg_subject']) && !empty($_POST['msg_message']))
{
	$name         = $_POST['msg_name'];
	$email        = $_POST['msg_email'];
	$subject      = $_POST['msg_subject'];
	$message      = $_POST['msg_message'];
	$check_result = apply_filters('gglcptch_verify_recaptcha', true, 'string');
	if (true === $check_result)
	{
		/* the reCAPTCHA answer is right */
		/* do necessary action */

		$to = get_option('admin_email');;

		if (strlen($name) == 0 || strlen($email) == 0 || strlen($subject) == 0 || strlen($message) == 0)
		{
			echo '<div class="alert  alert-warning" role="alert">' . __('Fill in all required fields.', 'stackdesign') . '</div>';
		}
		else
		{
			if (mail($to, $subject, __('From:', 'stackdesign') . $email . "\r\n" . __('Message:', 'stackdesign') . $message))
			{
				echo '<div class="alert alert-success" role="alert">' . __('Your message has been successfully sent.', 'stackdesign') . '</div>';
			}
			else
			{
				echo '<div class="alert alert-danger" role="alert">' . __('There was an error sending the message.', 'stackdesign') . '</div>';
			}
		}
	}
	else
	{
		echo '<div class="alert alert-danger" role="alert">' . __('Error in reCAPTCHA', 'stackdesign') . '</div>';
	}
}

?>

    <div class="row">

        <div class="col-sm-8 col-md-8 blog-main">
            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <form action="<?php echo the_permalink(); ?>" method="POST" name="my_contact_form">
                <div class="form-group">
                    <p><label for="msg_name"><?php _e('Name:', 'stackdesign'); ?> <span>*</span> <br></label><input
                                type="text" class="input" name="msg_name" value=""></p>
                    <p><label for="msg_email"><?php _e('Email:', 'stackdesign'); ?> <span>*</span> <br></label><input
                                type="text" class="input" name="msg_email" value=""></p>
                    <p><label for="msg_subject"><?php _e('Subject:', 'stackdesign'); ?> <span>*</span>
                            <br></label><input type="text" class="input" name="msg_subject" value=""></p>
                    <p><label for="msg_message"><?php _e('Message:', 'stackdesign'); ?> <span>*</span>
                            <br></label><textarea type="text" class="input" name="msg_message"></textarea></p>
					<?php echo apply_filters('gglcptch_display_recaptcha', ''); ?>
					<?php echo apply_filters('cptch_display', ''); ?>
                    <input type="hidden" name="action" value="1">
                    <p><input name="my_contact_form" class="button" type="submit"
                              value="<?php echo __('Send', 'stackdesign'); ?>"></p>
                </div>

            </form>

        </div>


		<?php get_sidebar(); ?>


    </div>


<?php get_footer(); ?>