<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>

<body>
<header class="blog-masthead">
    <div class="container">
        <div class="blog-header">
			<?php $custom_logo_id = get_theme_mod('custom_logo');
			$logo                 = wp_get_attachment_image_src($custom_logo_id, 'full');
			if (has_custom_logo())
			{
				echo '<img src="' . esc_url($logo[0]) . '"  >';
			}
			else
			{

				echo '<a  href="' . get_bloginfo('url') . '"><h1  class="blog-title" >' . get_bloginfo('name') . '</h1>';
				echo '<p class="lead  blog-description"></p></a>';


			} ?>
        </div>
    </div>
</header>

<nav class="navbar navbar-default" role="navigation">
    <div class="container"> <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
		<?php
		wp_nav_menu(array(
				'theme_location'  => 'primary',
				'depth'           => 2,
				'container'       => 'div',
				'container_class' => 'collapse navbar-collapse',
				'container_id'    => 'bs-example-navbar-collapse-1',
				'menu_class'      => 'nav navbar-nav',
				'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
				'walker'          => new WP_Bootstrap_Navwalker())
		);
		?>
    </div>
</nav>
<div class="container">

<?php
$license_stackdesign = get_theme_mod('license_stackdesgin', 'default');
$result              = strcmp($license_stackdesign, "UGVyc2lhbiBib290c3RyYXAgd29yZHByZXNzIHRlbXBsYXRlIDEuNw==");
if ($result !== 0)
{
	echo '<div class="alert alert-warning" role="alert">' .
		__('You are using an invalid license, it is warning message.Please buy a valid license to access updates and other features. 
                your license number:', 'stackdesign')
		. $license_stackdesign . '
                       <a  href="/" title="buy">' . __("Buy template", "stackdesign") . '</a>
                       </div><br>';

}
?>