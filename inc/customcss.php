/*
*Custom css  and export/import file file generator
*/


/*
*Add font to template
*/
@font-face {
font-family: 'byekan';
src: url('<?php echo site_url(); ?>/wp-content/themes/StackDesign-Wordpress-Theme/fonts/BYekan.eot') format('eot'), url('/wp-content/themes/StackDesign-Wordpress-Theme/fonts/BYekan.ttf') format('truetype');
}
@font-face {
font-family: 'droid sans';
src: url('<?php echo site_url(); ?>/wp-content/themes/StackDesign-Wordpress-Theme/fonts/BDroidSan.eot') format('eot'),url('<?php echo site_url(); ?>/wp-content/themes/StackDesign-Wordpress-Theme/fonts/DroidSan.tff')format('truetype');
}

@font-face {
font-family: 'sahel';
src: url('<?php echo site_url(); ?>/wp-content/themes/StackDesign-Wordpress-Theme/fonts/Sahel.eot') format('eot'), url('<?php echo site_url(); ?>/wp-content/themes/StackDesign-Wordpress-Theme/fonts/Sahel.ttf')format('truetype');
}

@font-face {
font-family: 'vazir';
src: url('<?php echo site_url(); ?>/wp-content/themes/StackDesign-Wordpress-Theme/fonts/Vazir.eot') format('eot'), url('<?php echo site_url(); ?>/wp-content/themes/StackDesign-Wordpress-Theme/fonts/Vazir.ttf')format('truetype');
}

<?php
require_once get_template_directory() . '/inc/customizer.php';
/**
 * Enqueues front-end CSS for color scheme.
 *
 * @since Stack Design 0.9
 *
 *
 */
global $link, $preview;

/*
 * Preview handle
 */
if ($preview == 1)
{

	$parameter_import = $link;
	$colors           = parameters_template_import($parameter_import);
	$color_scheme_css = stackdesign_get_color_scheme_css($colors);
	echo $color_scheme_css;

	return 0;
}
/*
* Import file handle
*/
$check_use_import_file_for_template = get_theme_mod('check_use_import_file_for_template');
if ($check_use_import_file_for_template == 1)
{
	$parameter_import = get_theme_mod('parameter_import');
	$colors           = parameters_template_import($parameter_import);
	$color_scheme_css = stackdesign_get_color_scheme_css($colors);
	echo $color_scheme_css;
	parameters_template_export($colors);

	return 0;

}


/*
* Css for user defined color schemes
*/
$change_to_default_colors_checkbox = get_theme_mod('change_to_default_colors_checkbox', 'default');
if ($change_to_default_colors_checkbox == 0)
{

	// 'body_background_color'            => $color_scheme[0],
	// 'header_background_color'     => $color_scheme[1],
	// 'theme_background_color'        => $color_scheme[2],
	// 'theme_text_color'                   => $color_scheme[3],
	// 'sidebar_widget_inner_background_color'=>$color_scheme[4] ,
	// 'menu_background_color'  => $color_scheme[5],
	// 'active_menu_background_color' => $color_scheme[6],
	// 'form_background_color' => $color_scheme[7],
	// 'footer_background_color' => $color_scheme[8],
	// 'ordinary_text_color'   => $color_scheme[9],
	// 'link_text_color'    => $color_scheme[10],
	// 'hover_link_text_color'=> $color_scheme[11],
	// 'header_text_color' => $color_scheme[12],
	// 'sidebar_widget_text_color' => $color_scheme[13],
	// 'menu_text_color'  => $color_scheme[14],
	//   'form_text_color' => $color_scheme[15],
	//   'footer_text_color' => $color_scheme[16],
	$body_background_color                 = get_theme_mod('body_background_color');
	$header_background_color               = get_theme_mod('header_background_color');
	$theme_background_color                = get_theme_mod('theme_background_color');
	$theme_text_color                      = get_theme_mod('theme_text_color');
	$sidebar_widget_inner_background_color = get_theme_mod('sidebar_widget_inner_background_color');
	$menu_background_color                 = get_theme_mod('menu_background_color');
	$active_menu_background_color          = get_theme_mod('active_menu_background_color');
	$form_background_color                 = get_theme_mod('form_background_color');
	$footer_background_color               = get_theme_mod('footer_background_color');
	$ordinary_text_color                   = get_theme_mod('ordinary_text_color');
	$link_text_color                       = get_theme_mod('link_text_color');
	$hover_link_text_color                 = get_theme_mod('hover_link_text_color');
	$header_text_color                     = get_theme_mod('header_text_color');
	$sidebar_widget_text_color             = get_theme_mod('sidebar_widget_text_color');
	$menu_text_color                       = get_theme_mod('menu_text_color');
	$form_text_color                       = get_theme_mod('form_text_color');
	$footer_text_color                     = get_theme_mod('footer_text_color');
	$font_family                           = get_theme_mod('font_family');
	$font_size                             = get_theme_mod('font_size');
	$line_height                           = get_theme_mod('line_height');
	$font_size_px                          = $font_size . 'px';
	$font_size_h1                          = ($font_size + 2) . 'px';
	$font_size_h2                          = ($font_size + 0) . 'px';
	$font_size_h3                          = ($font_size + 0) . 'px';

	$colors = array(
		'body_background_color'                 => $body_background_color,
		'header_background_color'               => $header_background_color,
		'theme_background_color'                => $theme_background_color,
		'theme_text_color'                      => $theme_text_color,
		'sidebar_widget_inner_background_color' => $sidebar_widget_inner_background_color,
		'menu_background_color'                 => $menu_background_color,
		'active_menu_background_color'          => $active_menu_background_color,
		'form_background_color'                 => $form_background_color,
		'footer_background_color'               => $footer_background_color,
		'ordinary_text_color'                   => $ordinary_text_color,
		'link_text_color'                       => $link_text_color,
		'hover_link_text_color'                 => $hover_link_text_color,
		'header_text_color'                     => $header_text_color,
		'sidebar_widget_text_color'             => $sidebar_widget_text_color,
		'menu_text_color'                       => $menu_text_color,
		'form_text_color'                       => $form_text_color,
		'footer_text_color'                     => $footer_text_color,
		'font_family'                           => $font_family,
		'font_size'                             => $font_size,
		'line_height'                           => $line_height,
		'font_size_px'                          => $font_size_px,
		'font_size_h1'                          => $font_size_h1,
		'font_size_h2'                          => $font_size_h2,
		'font_size_h3'                          => $font_size_h3,

	);

	$color_scheme_css = stackdesign_get_color_scheme_css($colors);
	echo $color_scheme_css;
	parameters_template_export($colors);

	return 0;
}

/*
* Css for default color schemes
*/
$color_scheme_option = get_theme_mod('color_scheme', 'default');

// Don't do anything if the default color scheme is selected.
if ('default' === $color_scheme_option)
{
	return;
}

$color_scheme = stackdesign_get_color_scheme();
$font_family  = get_theme_mod('font_family');
$font_size    = get_theme_mod('font_size');
$line_height  = get_theme_mod('line_height');
$font_size_px = $font_size . 'px';
$font_size_h1 = ($font_size + 2) . 'px';
$font_size_h2 = ($font_size + 0) . 'px';
$font_size_h3 = ($font_size + 0) . 'px';

// Convert main and sidebar text hex color to rgba.

$colors = array(
	'body_background_color'                 => $color_scheme[0],
	'header_background_color'               => $color_scheme[1],
	'theme_background_color'                => $color_scheme[2],
	'theme_text_color'                      => $color_scheme[3],
	'sidebar_widget_inner_background_color' => $color_scheme[4],
	'menu_background_color'                 => $color_scheme[5],
	'active_menu_background_color'          => $color_scheme[6],
	'form_background_color'                 => $color_scheme[7],
	'footer_background_color'               => $color_scheme[8],
	'ordinary_text_color'                   => $color_scheme[9],
	'link_text_color'                       => $color_scheme[10],
	'hover_link_text_color'                 => $color_scheme[11],
	'header_text_color'                     => $color_scheme[12],
	'sidebar_widget_text_color'             => $color_scheme[13],
	'menu_text_color'                       => $color_scheme[14],
	'form_text_color'                       => $color_scheme[15],
	'footer_text_color'                     => $color_scheme[16],
	'font_family'                           => $font_family,
	'font_size'                             => $font_size,
	'line_height'                           => $line_height,
	'font_size_px'                          => $font_size_px,
	'font_size_h1'                          => $font_size_h1,
	'font_size_h2'                          => $font_size_h2,
	'font_size_h3'                          => $font_size_h3,

);


$color_scheme_css = stackdesign_get_color_scheme_css($colors);
if (get_locale() == ('fa_IR'))
{
	echo $color_scheme_css;
}
else
{
	echo $color_scheme_css;
}
parameters_template_export($colors);


/*
* Function for create export file
*/
function parameters_template_export($colors)
{

	$exports      = json_encode($colors);
	$filename     = 'export_theme';
	$export_theme = fopen(TEMPLATEPATH . "/" . $filename . ".txt", "w");
	fputs($export_theme, $exports);
	fclose($export_theme);

}

/*
 * Function for handling import file
 */
function parameters_template_import($parameter_import)
{

	$url_shortener_setting = get_theme_mod('url_shortener_setting');

	if ($url_shortener_setting == 1)
	{
		$filename = get_site_url() . $parameter_import;
	}
	else
	{
		$filename = $parameter_import;
	}
	$colors = json_decode(file_get_contents($filename), true);

	return $colors;
}


?>



