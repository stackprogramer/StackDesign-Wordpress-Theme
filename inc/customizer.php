<?php
/**
 * Contains methods for customizing the theme customization screen.
 *
 * @link  http://codex.wordpress.org/Theme_Customization_API
 * @since StackDesign .95
 */

class StackDesign_Customize
{

	/**
	 * This hooks into 'customize_register' (available as of WP 3.4) and allows
	 * you to add new sections and controls to the Theme Customize screen.
	 *
	 * Note: To enable instant preview, we have to actually write a bit of custom
	 * javascript. See live_preview() for more.
	 *
	 * @see   add_action('customize_register',$func)
	 *
	 * @param \WP_Customize_Manager $wp_customize
	 *
	 * @link  http://ottopress.com/2012/how-to-leverage-the-theme-customizer-in-your-own-themes/
	 * @since StackDesign 1.0
	 */


	public static function register($wp_customize)
	{
		/*
		 * Returning setting
		 */
		$color_schemes                     = stackdesign_get_color_schemes();
		$color_scheme_option               = get_theme_mod('color_scheme', 'default');
		$change_to_default_colors_checkbox = get_theme_mod('change_to_default_colors_checkbox', 'default');
		$copyright_footer_text             = get_theme_mod('copyright_footer_text', 'default');

		//echo 'color schemes:'. $color_schemes[$color_scheme_option]['colors'][0].$color_scheme_option;
		// 'body_background_color'            => $color_scheme[0],
		// 'header_background_color'     => $color_scheme[1],
		// 'theme_background_color'        => $color_scheme[2],
		// 'theme_text_color'                   => $color_scheme[3],
		// 'sidebar_widget_inner_background_color'=>$color_scheme[4] ,
		// 'menu_background_color'  => $color_scheme[5],
		// 'active_menu_background_color' => $color_scheme[6],
		// 'form_background_color' => $color_scheme[7],
		// 'footer_background_color' => $color_scheme[8],
		// 'ordinary_text_color'   => $color_scheme[9],
		// 'link_text_color'    => $color_scheme[10],
		// 'hover_link_text_color'=> $color_scheme[11],
		// 'header_text_color' => $color_scheme[12],
		// 'sidebar_widget_text_color' => $color_scheme[13],
		// 'menu_text_color'  => $color_scheme[14],
		//   'form_text_color' => $color_scheme[15],
		//   'footer_text_color' => $color_scheme[16],

		/*
		 * Section color, we defined colors of  template setting
		 */
		//1. Define a new section (if desired) to the Theme Customizer
		$wp_customize->add_section('color',
			array(
				'title'       => __('Stackdesign Options', 'stackdesign'), //Visible title of section
				'priority'    => 35, //Determines what order this appears in
				'capability'  => 'edit_theme_options', //Capability needed to tweak
				'description' => __('Allows you to customize some example settings for StackDesign.', 'stackdesign'), //Descriptive tooltip
			)
		);


		// Add color scheme setting and control.
		$wp_customize->add_setting(
			'color_scheme', array(
				'default'           => 'default',
				'sanitize_callback' => 'stackdesign_sanitize_color_scheme',
				'transport'         => 'postMessage',
			)
		);


		$wp_customize->add_control(
			'color_scheme', array(
				'label'    => __('Base Color Scheme', 'stackdesign'),
				'section'  => 'colors',
				'type'     => 'select',
				'choices'  => stackdesign_get_color_scheme_choices(),
				'priority' => 1,
			)
		);

		$wp_customize->add_setting('change_to_default_colors_checkbox',
			array(
				'default'   => 1,
				'transport' => 'refresh',
			)
		);

		$wp_customize->add_control('change_to_default_colors_checkbox',
			array(
				'label'       => __('Change To Default Colors Checkbox', 'stackdesign'),
				'description' => __('With checked checkbox setting colors change to default colors', 'stackdesign'),
				'section'     => 'colors',
				'priority'    => 1, // Optional. Order priority to load the control. Default: 10
				'type'        => 'checkbox',
				'capability'  => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
			)
		);


		/*
		 * 0-body_background_color
		 */
		$wp_customize->add_setting('body_background_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][0], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);


		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_body_background_color', //Set a unique ID for the control
			array(
				'label'    => __('Body Background Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'body_background_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 5, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));


		/*
		 * 1-header_background_color
		 */
		$wp_customize->add_setting('header_background_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][1], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_header_background_color', //Set a unique ID for the control
			array(
				'label'    => __('Header Background Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'header_background_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 5, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));

		/*
		 * 2-theme_background_color
		 */

		$wp_customize->add_setting('theme_background_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][2], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_theme_background_color', //Set a unique ID for the control
			array(
				'label'    => __('Theme Background Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'theme_background_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 6, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));

		/*
		 * 3-theme_text_color
		 */
		$wp_customize->add_setting('theme_text_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][3], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_theme_text_color', //Set a unique ID for the control
			array(
				'label'    => __('Theme Text Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'theme_text_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 7, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));

		/*
		 * 4-sidebar_widget_inner_background_color
		 */
		$wp_customize->add_setting('sidebar_widget_inner_background_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][4], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_theme_textcolor2', //Set a unique ID for the control
			array(
				'label'    => __('Sidebar Widget Inner Background Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'sidebar_widget_inner_background_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 7, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));

		/*
		 * 5-menu_background_color
		 */
		$wp_customize->add_setting('menu_background_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][5], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_menu_background_color', //Set a unique ID for the control
			array(
				'label'    => __('Menu Background Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'menu_background_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 7, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));


		/*
		 * 6-active_menu_background_color
		 */
		$wp_customize->add_setting('active_menu_background_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][6], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_active_menu_background_color', //Set a unique ID for the control
			array(
				'label'    => __('Active Menu Background Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'active_menu_background_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 7, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));


		/*
		 * 7-form_background_color
		 */
		$wp_customize->add_setting('form_background_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][7], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_form_background_color', //Set a unique ID for the control
			array(
				'label'    => __('Form background Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'form_background_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 7, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));


		/*
		 * 8-footer_background_color
		 */
		$wp_customize->add_setting('footer_background_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][8], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_footer_background_color', //Set a unique ID for the control
			array(
				'label'    => __('Footer Background Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'footer_background_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 7, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));


		/*
		 * 9-ordinary_text_color
		 */
		$wp_customize->add_setting('ordinary_text_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][9], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_ordinary_text_color', //Set a unique ID for the control
			array(
				'label'    => __('Ordinary Text Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'ordinary_text_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 7, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));


		/*
		 * 10-link_text_color
		 */
		$wp_customize->add_setting('link_text_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][10], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_link_text_color', //Set a unique ID for the control
			array(
				'label'    => __('Link Text Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'link_text_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 7, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));


		/*
		 * 11-hover_link_text_color
		 */
		$wp_customize->add_setting('hover_link_text_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][11], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_hover_link_text_color', //Set a unique ID for the control
			array(
				'label'    => __('Hover Link text Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'hover_link_text_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 7, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));


		/*
		 * 12-header_text_color
		 */
		$wp_customize->add_setting('header_text_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][12], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_header_text_color', //Set a unique ID for the control
			array(
				'label'    => __('Header Text Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'header_text_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 7, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));


		/*
		 * 13-sidebar_widget_text_color
		 */
		$wp_customize->add_setting('sidebar_widget_text_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][13], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_sidebar_widget_text_color', //Set a unique ID for the control
			array(
				'label'    => __('Sidebar Widget Text Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'sidebar_widget_text_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 7, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));


		/*
		 * 14-menu_text_color
		 */
		$wp_customize->add_setting('menu_text_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][14], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_menu_text_color', //Set a unique ID for the control
			array(
				'label'    => __('Menu Text Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'menu_text_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 7, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));


		/*
		 * 15-form_text_color
		 */
		$wp_customize->add_setting('form_text_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][15], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'stackdesign_form_text_color', //Set a unique ID for the control
			array(
				'label'    => __('Form Text Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'form_text_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 7, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));


		/*
		* 16-footer_text_color
		*/
		$wp_customize->add_setting('footer_text_color', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => $color_schemes[$color_scheme_option]['colors'][16], //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Color_Control( //Instantiate the color control class
			$wp_customize, //Pass the $wp_customize object (required)
			'footer_text_color', //Set a unique ID for the control
			array(
				'label'    => __('Footer Text Color', 'stackdesign'), //Admin-visible name of the control
				'settings' => 'footer_text_color', //Which setting to load and manipulate (serialized is okay)
				'priority' => 7, //Determines the order this control appears in for the specified section
				'section'  => 'colors', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
			)
		));
		/*
		 * Section url shortener
		 */
		$wp_customize->add_section('url_shortener_section', array(
			'title'    => __('Relative url activation', 'stackdesign'),
			'priority' => 100
		));

		$wp_customize->add_setting('url_shortener_setting', array(
			'default'    => '0',
			'type'       => 'theme_mod',
			'capability' => 'edit_theme_options', //Capability needed to tweak
			'transport'  => 'refresh',

		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'url_shortener_control',
				array(
					'label'    => __('Active relative url for  your template', 'stackdesign'),
					'section'  => 'url_shortener_section',
					'settings' => 'url_shortener_setting',
					'type'     => 'checkbox',
				)
			)
		);


		/*
		* Section font
		*/
		$wp_customize->add_section('font_options',
			array(
				'title'       => __('Font Options', 'stackdesign'), //Visible title of section
				'priority'    => 20, //Determines what order this appears in
				'capability'  => 'edit_theme_options', //Capability needed to tweak
				'description' => __('Allows you to customize settings for font.', 'stackdesign'), //Descriptive tooltip
			)
		);

		$wp_customize->add_setting('font_family', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => 'sahel', //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				//'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Control(
			$wp_customize, //Pass the $wp_customize object (required)
			'font_family_stackdesigne', //Set a unique ID for the control
			array(
				'label'       => __('Select font theme', 'stackdesign'), //Admin-visible name of the control
				'description' => __('Using this option you can change the fonts', 'stackdesign'),
				'settings'    => 'font_family', //Which setting to load and manipulate (serialized is okay)
				'priority'    => 10, //Determines the order this control appears in for the specified section
				'section'     => 'font_options', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
				'type'        => 'select',
				'choices'     => array(
					'sahel'        => 'Sahel',
					'byekan'       => 'BYekan',
					'droid sans'   => 'Droid sans',
					'vazir'        => 'Vazir',
					'iranian sans' => 'Iranian Sans',

				)
			)
		));

		$wp_customize->add_setting('font_size', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => '16', //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				//'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Control(
			$wp_customize, //Pass the $wp_customize object (required)
			'font_size_stackdesign', //Set a unique ID for the control
			array(
				'label'       => __('Select font size of theme', 'stackdesign'), //Admin-visible name of the control
				'description' => __('Using this option you can change the font size', 'stackdesign'),
				'settings'    => 'font_size', //Which setting to load and manipulate (serialized is okay)
				'priority'    => 10, //Determines the order this control appears in for the specified section
				'section'     => 'font_options', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
				'type'        => 'select',
				'choices'     => array(
					'10' => '10px',
					'12' => '12px',
					'14' => '14px',
					'16' => '16px',
					'18' => '18px',
					'20' => '20px',


				)
			)
		));


		$wp_customize->add_setting('line_height', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
			array(
				'default'    => '2', //Default setting/value to save
				'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
				'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
				//'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
			)
		);

		$wp_customize->add_control(new WP_Customize_Control(
			$wp_customize, //Pass the $wp_customize object (required)
			'lineheight_stackdesign', //Set a unique ID for the control
			array(
				'label'       => __('Select line height of theme', 'stackdesign'), //Admin-visible name of the control
				'description' => __('Using this option you can change the line height', 'stackdesign'),
				'settings'    => 'line_height', //Which setting to load and manipulate (serialized is okay)
				'priority'    => 10, //Determines the order this control appears in for the specified section
				'section'     => 'font_options', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
				'type'        => 'select',
				'choices'     => array(
					'1'   => '1',
					'1.1' => '1.1',
					'1.2' => '1.2',
					'1.3' => '1.3',
					'1.4' => '1.4',
					'1.4' => '1.4',
					'1.5' => '1.5',
					'1.6' => '1.6',
					'1.7' => '1.7',
					'1.8' => '1.8',
					'1.9' => '1.9',
					'2'   => '2',
					'2.1' => '2.1',
					'2.2' => '2.2',
				)
			)
		));

		/*
		* Section About template
		*/
		$wp_customize->add_section('about_template_stackdesign', array(
			'title'    => __('About stackdesign template', 'stackdesign'),
			'priority' => 100,
		));


		$wp_customize->add_setting('license_stackdesgin', array(
			'default'   => 'UGVyc2lhbiBib290c3RyYXAgd29yZHByZXNzIHRlbXBsYXRlIDEuNw==',
			'transport' => 'refresh',
			'type'      => 'theme_mod',

		));
		$info = " قالب استک دیزاین با هدف ساخت قالبی زیبا و واکنش گرا برای وبلاگ های فارسی و انگلیسی زبان طراحی شده است. در طراحی این قالب از فریم ورک معروف سی اس اس یعنی بوت 
	    استرپ استفاده شده است. اولین نسخه این قالب در ژوئن 
	   سال ۲۰۱۸ به صورت رایگان منتشر شده است لایسنس 2019:";
		$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'about_stackdesgin', array(
			'label'    => __($info, 'stackdesign'),
			'section'  => 'about_template_stackdesign',
			'settings' => 'license_stackdesgin',
			'type'     => 'text',
		)));
		/*
		* Google analytic code template
		*/
		$wp_customize->add_section('google_analytic_stackdesign', array(
			'title'    => __('Google analytic code', 'stackdesign'),
			'priority' => 100,
		));

		$wp_customize->add_setting('enable_google_analytic_setting', array(
			'default'    => '0',
			'type'       => 'theme_mod',
			'capability' => 'edit_theme_options', //Capability needed to tweak
			'transport'  => 'refresh',

		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'google_analytic_code_control',
				array(
					'label'    => __('Active google analytic code', 'stackdesign'),
					'section'  => 'google_analytic_stackdesign',
					'settings' => 'enable_google_analytic_setting',
					'type'     => 'checkbox',
				)
			)
		);

		$wp_customize->add_setting('google_analytic_code_stackdesgin', array(
			'default'   => 'UA-85028641-2',
			'transport' => 'refresh',
			'type'      => 'theme_mod',

		));

		$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'google_analytic_stackdesgin', array(
			'label'    => __('Enter your google analytic code for your domian', 'stackdesign'),
			'section'  => 'google_analytic_stackdesign',
			'settings' => 'google_analytic_code_stackdesgin',
			'type'     => 'text',
		)));


		/*
		 * Section header_footer
		 */
		$wp_customize->add_section('header_footer',
			array(
				'title'       => __('Header and Footer', 'stackdesign'), //Visible title of section
				'priority'    => 35, //Determines what order this appears in
				'capability'  => 'edit_theme_options', //Capability needed to tweak
				'description' => __('Allows you to customize some example settings for header and footer StackDesign theme.', 'stackdesign'), //Descriptive tooltip
			)
		);


		$wp_customize->add_setting('copyright_footer_text',
			array(
				'default'   => 'وبلاگ استک دیزاین 2017-2019©',
				'transport' => 'refresh',
			)
		);

		$wp_customize->add_control('copyright_footer_text',
			array(
				'label'       => __('Copyright footer text'),
				'description' => esc_html__('Type copyright footer text'),
				'section'     => 'header_footer',
				'priority'    => 10, // Optional. Order priority to load the control. Default: 10
				'type'        => 'text', // Can be either text, email, url, number, hidden, or date
				'capability'  => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
				'input_attrs' => array( // Optional.
					'class'       => 'my-custom-class',
					'style'       => 'border: 1px solid rebeccapurple',
					'placeholder' => __('Enter copyright footer text...'),
				),
			)
		);


		/*
		 * Customizer export
		 * This section gererate customizer template option to a output file
		 */
		$wp_customize->add_section('theme_parameter_export', array(
			'capability' => 'edit_theme_options',
			'title'      => __('Export/Import parameters', 'stackdesign'),
			'priority'   => 100,
		));

		$wp_customize->add_setting('parameter_export', array(
			'capability' => 'edit_theme_options',
			'type'       => 'hidden',
			'autoload'   => false
		));

		$wp_customize->add_control('parameter_export',
			array(
				'label'       => __('Download export file', 'stackdesign'),
				'description' => stackdesign_get_link_export_file(),
				'section'     => 'theme_parameter_export',
				'priority'    => 10, // Optional. Order priority to load the control. Default: 10
				'type'        => 'hidden', // Can be either text, email, url, number, hidden, or date
				'capability'  => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
			)
		);


		$wp_customize->add_setting('check_use_import_file_for_template',
			array(
				'default'   => 0,
				'transport' => 'refresh',
			)
		);

		$wp_customize->add_control('check_use_import_file_for_template',
			array(
				'label'       => __('Use import file for template', 'stackdesign'),
				'description' => __('With checked checkbox setting colors change according import file', 'stackdesign'),
				'section'     => 'theme_parameter_export',
				'priority'    => 95, // Optional. Order priority to load the control. Default: 10
				'type'        => 'checkbox',
				'capability'  => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
			)
		);
		$wp_customize->add_setting('parameter_import', array(
			'transport' => 'refresh',
			'type'      => 'theme_mod',
		));

		$wp_customize->add_control(
			new WP_Customize_Upload_Control(
				$wp_customize,
				'your_setting_id',
				array(
					'label'    => __('Import parameters file for template', 'stackdesign'),
					'section'  => 'theme_parameter_export',
					'settings' => 'parameter_import',
				))
		);

		/*
		* We can also change built-in settings by modifying properties. For instance, let's make some stuff use live preview JS...
		*/
		$wp_customize->get_setting('blogname')->transport         = 'postMessage';
		$wp_customize->get_setting('blogdescription')->transport  = 'postMessage';
		$wp_customize->get_setting('header_textcolor')->transport = 'postMessage';
		$wp_customize->get_setting('background_color')->transport = 'postMessage';


	}

	/**
	 * This will output the custom WordPress settings to the live theme's WP head.
	 *
	 * Used by hook: 'wp_head'
	 *
	 * @see   add_action('wp_head',$func)
	 * @since StackDesign 1.0
	 */
	public static function header_output()
	{

		$change_to_default_colors_checkbox = get_theme_mod('change_to_default_colors_checkbox', 'default');

		if ($change_to_default_colors_checkbox == 1)
		{
			return 0;
		}

	}

	/**
	 * This outputs the javascript needed to automate the live settings preview.
	 * Also keep in mind that this function isn't necessary unless your settings
	 * are using 'transport'=>'postMessage' instead of the default 'transport'
	 * => 'refresh'
	 *
	 * Used by hook: 'customize_preview_init'
	 *
	 * @see   add_action('customize_preview_init',$func)
	 * @since StackDesign 1.0
	 */
	public static function live_preview()
	{
		wp_enqueue_script(
			'StackDesign-themecustomizer', // Give the script a unique ID
			get_template_directory_uri() . '/js/theme-customizer.js', // Define the path to the JS file
			array('jquery', 'customize-preview'), // Define dependencies
			'', // Define a version (optional)
			true // Specify whether to put in footer (leave this true)
		);
	}

	/**
	 * This will generate a line of CSS for use in header output. If the setting
	 * ($mod_name) has no defined value, the CSS will not be output.
	 *
	 * @uses  get_theme_mod()
	 *
	 * @param string $selector CSS selector
	 * @param string $style    The name of the CSS *property* to modify
	 * @param string $mod_name The name of the 'theme_mod' option to fetch
	 * @param string $prefix   Optional. Anything that needs to be output before the CSS property
	 * @param string $postfix  Optional. Anything that needs to be output after the CSS property
	 * @param bool   $echo     Optional. Whether to print directly to the page (default: true).
	 *
	 * @return string Returns a single line of CSS with selectors and a property.
	 * @since StackDesign 1.0
	 */
	public static function generate_css($selector, $style, $mod_name, $prefix = '', $postfix = '', $echo = true)
	{
		$return = '';
		$mod    = get_theme_mod($mod_name);
		if (!empty($mod))
		{
			$return = sprintf('%s { %s:%s; }',
				$selector,
				$style,
				$prefix . $mod . $postfix
			);
			if ($echo)
			{
				echo $return;
			}
		}

		return $return;
	}
}

// Setup the Theme Customizer settings and controls...
add_action('customize_register', array('StackDesign_Customize', 'register'));

// Output custom CSS to live site
add_action('wp_head', array('StackDesign_Customize', 'header_output'));

// Enqueue live preview javascript in Theme Customizer admin screen
add_action('customize_preview_init', array('StackDesign_Customize', 'live_preview'));


/**
 * Register color schemes for Stack Design.
 *
 * Can be filtered with {@see 'stackdesign_color_schemes'}.
 *
 * The order of colors in a colors array:
 * 1. Main Background Color.
 * 2. Sidebar Background Color.
 * 3. Box Background Color.
 * 4. Main Text and Link Color.
 * 5. Sidebar Text and Link Color.
 * 6. Meta Box Background Color.
 *
 * @since Stack Design 0.9
 *
 * @return array An associative array of color scheme options.
 */
function stackdesign_get_color_schemes()
{
	/**
	 * Filter the color schemes registered for use with Stack Design.
	 *
	 * The default schemes include 'default', 'dark', 'yellow', 'pink', 'purple', and 'blue'.
	 *
	 * @since Stack Design 0.9
	 *
	 * @param array $schemes {
	 *                       Associative array of color schemes data.
	 *
	 * @type array  $slug    {
	 *         Associative array of information for setting up the color scheme.
	 *
	 * @type string $label   Color scheme label.
	 * @type array  $colors  HEX codes for default colors prepended with a hash symbol ('#').
	 *                              Colors are defined in the following order: Main background, sidebar
	 *                              background, box background, main text and link, sidebar text and link,
	 *                              meta box background.
	 *     }
	 * }
	 */
	return apply_filters(
		'stackdesign_color_schemes', array(
			'default'  => array(
				'label'  => __('Default', 'stackdesign'),
				'colors' => array(
					'#ffffff',
					'#337ab7',
					'#337ab7',
					'#ffffff',
					'#ffffff',
					'#f9f9f9',
					'#d2d2d2',
					'#ffffff',
					'#f9f9f9',
					'#000000',
					'#337ab7',
					'#337af0',
					'#ffffff',
					'#000000',
					'#000000',
					'#000000',
					'#000000',
				),
			),
			'Lighblue' => array(
				'label'  => __('Light blue', 'stackdesign'),
				'colors' => array(
					'#ffffff',
					'#5bc0de',
					'#5bc0de',
					'#ffffff',
					'#ffffff',
					'#e7e7e7',
					'#d2d2d2',
					'#ffffff',
					'#e7e7e7',
					'#000000',
					'#5bc0de',
					'#337af0',
					'#ffffff',
					'#000000',
					'#000000',
					'#000000',
					'#000000',
				),
			),
			'green'    => array(
				'label'  => __('Green', 'stackdesign'),
				'colors' => array(
					'#ffffff',
					'#5cb85c',
					'#5cb85c',
					'#ffffff',
					'#ffffff',
					'#e7e7e7',
					'#d2d2d2',
					'#ffffff',
					'#e7e7e7',
					'#000000',
					'#5cb85c',
					'#337af0',
					'#ffffff',
					'#000000',
					'#000000',
					'#000000',
					'#000000',
				),
			),
			'Oranage'  => array(
				'label'  => __('Oranage', 'stackdesign'),
				'colors' => array(
					'#ffffff',
					'#e67e22',
					'#e67e22',
					'#ffffff',
					'#ffffff',
					'#e7e7e7',
					'#d2d2d2',
					'#ffffff',
					'#e7e7e7',
					'#000000',
					'#e67e22',
					'#337af0',
					'#ffffff',
					'#000000',
					'#000000',
					'#000000',
					'#000000',
				),
			),
			'darkblue' => array(
				'label'  => __('Dark blue', 'stackdesign'),
				'colors' => array(
					'#ffffff',
					'#34495e',
					'#34495e',
					'#ffffff',
					'#ffffff',
					'#e7e7e7',
					'#d2d2d2',
					'#ffffff',
					'#e7e7e7',
					'#000000',
					'#34495e',
					'#337af0',
					'#ffffff',
					'#000000',
					'#000000',
					'#000000',
					'#000000',
				),
			),
			'black'    => array(
				'label'  => __('Black', 'stackdesign'),
				'colors' => array(
					'#ffffff',
					'#000000',
					'#000000',
					'#ffffff',
					'#ffffff',
					'#e7e7e7',
					'#d2d2d2',
					'#ffffff',
					'#e7e7e7',
					'#000000',
					'#000000',
					'#337af0',
					'#ffffff',
					'#000000',
					'#000000',
					'#000000',
					'#000000',
				),
			),

			'red' => array(
				'label'  => __('Red', 'stackdesign'),
				'colors' => array(
					'#ffffff',
					'#e74c3c',
					'#e74c3c',
					'#ffffff',
					'#ffffff',
					'#e7e7e7',
					'#d2d2d2',
					'#ffffff',
					'#e7e7e7',
					'#000000',
					'#e74c3c',
					'#337af0',
					'#ffffff',
					'#000000',
					'#000000',
					'#000000',
					'#000000',
				),
			),

			'facebook' => array(
				'label'  => __('Facebook', 'stackdesign'),
				'colors' => array(
					'#ffffff',
					'#3b5998',
					'#3b5998',
					'#ffffff',
					'#ffffff',
					'#e7e7e7',
					'#d2d2d2',
					'#ffffff',
					'#e7e7e7',
					'#000000',
					'#3b5998',
					'#337af0',
					'#ffffff',
					'#000000',
					'#000000',
					'#000000',
					'#000000',
				),
			),

			'qt' => array(
				'label'  => __('Qt', 'stackdesign'),
				'colors' => array(
					'#ffffff',
					'#41cd52',
					'#41cd52',
					'#ffffff',
					'#ffffff',
					'#e7e7e7',
					'#d2d2d2',
					'#ffffff',
					'#e7e7e7',
					'#000000',
					'#41cd52',
					'#337af0',
					'#ffffff',
					'#000000',
					'#000000',
					'#000000',
					'#000000',
				),
			),

			'visualstudio' => array(
				'label'  => __('Visual Studio', 'stackdesign'),
				'colors' => array(
					'#ffffff',
					'#865fc5',
					'#865fc5',
					'#ffffff',
					'#ffffff',
					'#e7e7e7',
					'#d2d2d2',
					'#ffffff',
					'#e7e7e7',
					'#000000',
					'#865fc5',
					'#337af0',
					'#ffffff',
					'#000000',
					'#000000',
					'#000000',
					'#000000',
				),
			),

			'android' => array(
				'label'  => __('Android', 'stackdesign'),
				'colors' => array(
					'#ffffff',
					'#a4c639',
					'#a4c639',
					'#ffffff',
					'#ffffff',
					'#e7e7e7',
					'#d2d2d2',
					'#ffffff',
					'#e7e7e7',
					'#000000',
					'#a4c639',
					'#337af0',
					'#ffffff',
					'#000000',
					'#000000',
					'#000000',
					'#000000',
				),
			),

			'farsi sadra' => array(
				'label'  => __('farsi sadra', 'stackdesign'),
				'colors' => array(
					'#ffffff',
					'#a8765d',
					'#a8765d',
					'#ffffff',
					'#ffffff',
					'#e7e7e7',
					'#d2d2d2',
					'#ffffff',
					'#e7e7e7',
					'#000000',
					'#a8765d',
					'#337af0',
					'#ffffff',
					'#000000',
					'#000000',
					'#000000',
					'#000000',
				),
			),
		)
	);
}


if (!function_exists('stackdesign_get_color_scheme')) :
	/**
	 * Get the current Stack Design color scheme.
	 *
	 * @since Stack Design 0.9
	 *
	 * @return array An associative array of either the current or default color scheme hex values.
	 */
	function stackdesign_get_color_scheme()
	{
		$color_scheme_option = get_theme_mod('color_scheme', 'default');
		$color_schemes       = stackdesign_get_color_schemes();

		if (array_key_exists($color_scheme_option, $color_schemes))
		{
			return $color_schemes[$color_scheme_option]['colors'];
		}

		//return $color_schemes['default']['colors'];
		return $color_schemes[$color_scheme_option]['colors'];

	}
endif; // stackdesign_get_color_scheme


if (!function_exists('stackdesign_get_color_scheme_choices')) :
	/**
	 * Returns an array of color scheme choices registered for Stack Design.
	 *
	 * @since Stack Design 0.9
	 *
	 * @return array Array of color schemes.
	 */
	function stackdesign_get_color_scheme_choices()
	{
		$color_schemes                = stackdesign_get_color_schemes();
		$color_scheme_control_options = array();

		foreach ($color_schemes as $color_scheme => $value)
		{
			$color_scheme_control_options[$color_scheme] = $value['label'];
		}

		return $color_scheme_control_options;
	}
endif; // stackdesign_get_color_scheme_choices

if (!function_exists('stackdesign_sanitize_color_scheme')) :
	/**
	 * Sanitization callback for color schemes.
	 *
	 * @since Stack Design 0.9
	 *
	 * @param string $value Color scheme name value.
	 *
	 * @return string Color scheme name.
	 */
	function stackdesign_sanitize_color_scheme($value)
	{
		$color_schemes = stackdesign_get_color_scheme_choices();

		if (!array_key_exists($value, $color_schemes))
		{
			$value = 'default';
		}

		return $value;
	}
endif; // stackdesign_sanitize_color_scheme


function stackdesign_get_color_scheme_css($colors)
{

//                $font_family= get_theme_mod( 'font_family' );
//                $font_size= get_theme_mod( 'font_size');
//                $line_height= get_theme_mod( 'line_height');
//                $font_size_px=$font_size.'px';
//                $font_size_h1=($font_size+2).'px';
//                $font_size_h2=($font_size+0).'px';
//                $font_size_h3=($font_size+0).'px';
	$colors = wp_parse_args(
		$colors, array(
			'body_background_color'                 => '',
			'header_background_color'               => '',
			'theme_background_color'                => '',
			'theme_text_color'                      => '',
			'sidebar_widget_inner_background_color' => '',
			'menu_background_color'                 => '',
			'active_menu_background_color'          => '',
			'form_background_color'                 => '',
			'footer_background_color'               => '',
			'ordinary_text_color'                   => '',
			'link_text_color'                       => '',
			'hover_link_text_color'                 => '',
			'header_text_color'                     => '',
			'sidebar_widget_text_color'             => '',
			'menu_text_color'                       => '',
			'form_background_color'                 => '',
			'footer_text_color'                     => '',
			'font_family'                           => '',
			'font_size'                             => '',
			'line_height'                           => '',
			'font_size_px'                          => '',
			'font_size_h1'                          => '',
			'font_size_h2'                          => '',
			'font_size_h3'                          => '',
		)
	);

//                $fonts = wp_parse_args(
//                    $fonts, array(
//                        'font_family' => '',
//                        'font_size' => '',
//                        'line_height'  => '',
//                        'font_size_px' =>'',
//                        'font_size_h1' => '',
//                        'font_size_h2'  => '',
//                        'font_size_h3' => '',
//                    )
//                );


	$css = <<<CSS
              /* Color Scheme */
            
              /* Background Color */
              body {
                background-color:{$colors['body_background_color']};
                color:{$colors['ordinary_text_color']};
                font-family:{$colors['font_family']} !important;
                font-size:{$colors['font_size_px']}!important;
                line-height:{$colors['line_height']}  !important;
              }
              h1,.h1{
              font-family:{$colors['font_family']} !important;
              line-height: {$colors['line_height']}  !important;
              font-size:{$colors['font_size_h1']}  !important;
              }
              h2,.h2{
               font-family:{$colors['font_family']} !important;
              font-size:{$colors['font_size_h2']}  !important;
               line-height: {$colors['line_height']}  !important;
              }
              h3, .h3, h4, .h4, h5, .h5, h6, .h6 {
               font-family:{$colors['font_family']}  !important;
              font-size:{$colors['font_size_h3']}  !important;
               line-height: {$colors['line_height']}  !important;
              }
            
              .blog-title,.blog-description,.blog-post-meta {
                  color:{$colors['header_text_color']};
                }
            
               .blog-masthead {
                background-color:{$colors['header_background_color']};
                border-color:{$colors['header_background_color']};
               }
            
               .blog-footer {
                 background-color:{$colors['footer_background_color']};
               }
              .widget-title,.fn,.corner-ribbon {
               background-color:{$colors['theme_background_color']};
               color:{$colors['theme_text_color']};
               }  
               #s,#cntctfrm_contact_name, #cntctfrm_contact_email, #cntctfrm_contact_subject,#cntctfrm_contact_message ,.input, .input-email, .input-text,.field-select,blockquote,.comment-body,#author, #email, #url, .cptch_input,#comment {
                 background-color:{$colors['form_background_color']};
                 color:{$colors['form_text_color']};
               }
               #searchsubmit,.cntctfrm_contact_submit,.button,.submit,.more-link{
                 background-color:{$colors['theme_background_color']};
                 border-color:{$colors['theme_background_color']};
                 color:{$colors['theme_text_color']};
              }
            
             .widget_recent_entries li,.widget_categories li,.widget_polylang li,.widget_recent_comments li,.widget_archive li {
                 background-color:{$colors['sidebar_widget_inner_background_color']};
                 color:{$colors['sidebar_widget_text_color']};
              }
              .sidebar-module-inset{
                 background-color:{$colors['sidebar_widget_inner_background_color']};
            }
            
            .navbar-default{
              background-color:{$colors['menu_background_color']};
              color:{$colors['menu_text_color']};
              border-color:{$colors['menu_background_color']};
            }
            .current_page_item,.navbar-default .navbar-nav .active a
             {
              background-color:{$colors['active_menu_background_color']};
            }
            .current_page_item:hover,.navbar-default .navbar-nav .active a:hover{
              background-color:{$colors['theme_background_color']};
              color:{$colors['theme_text_color']};
            }
            a{
              color:{$colors['link_text_color']};
            }
            a:hover{
              color:{$colors['hover_link_text_color']};
            }

CSS;

	return $css;
}

if (!function_exists('stackdesign_get_link_export_file')) :
	/**
	 * Create export file download lin
	 *
	 * @since Stack Design 1.7
	 *
	 */
	function stackdesign_get_link_export_file()
	{

		$links = array(
			array('url' => get_template_directory_uri() . '/export_theme.txt', 'text' => __('Export file', 'stackdesign'), 'desc' => __('Download export file', 'stackdesign')),
			array('url' => 'https://blog.stackprogramer.xyz/', 'text' => __('Typical Template Export files download', 'stackdesign'), 'desc' => __('See export files', 'stackdesign')),

		);

		$html = '';

		foreach ($links as $link)
		{
			$html .= '<p>' . $link['desc'] . '<br>' . PHP_EOL;
			$html .= sprintf('<a href="%s" target="_blank">%s</a>', $link['url'], $link['text']) . '<br>' . PHP_EOL;
			$html .= '</p>' . PHP_EOL;
		}


		return $html;
	}
endif; // stackdesign_get_link_export_file

