<?php
/**
 * Stack Design functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link       https://codex.wordpress.org/Theme_Development
 * @link       https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package    Stack Design
 * @subpackage Stack_Design
 * @since      StackDesign 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Stack Design 1.0
 */

/*
 * Function custom link for custom css and preview
 */
global $link, $preview, $flag_preview;
$preview      = 0;
$flag_preview = 0;
function my_custom_preview_request($wp)
{
	if (
		!empty($_GET['preview']) && !empty($_GET['link'])
		&& $_GET['preview'] == '1'
	)
	{
		global $link, $preview, $flag_preview;
		$flag_preview = 1;
		$link         = htmlspecialchars($_GET['link']);
		$preview      = htmlspecialchars($_GET['preview']);
	}
}

add_action('parse_request', 'my_custom_preview_request');

function my_custom_wp_request($wp)
{
	if (
		!empty($_GET['my-custom-content'])
		&& $_GET['my-custom-content'] == 'css'
	)
	{

		# get theme options
		header('Content-Type: text/css');
		require dirname(__FILE__) . '/inc/customcss.php';
		exit;
	}
}

add_action('parse_request', 'my_custom_wp_request');

/*
* set content width
*/
if (!isset($content_width))
{
	$content_width = 660;
}


/**
 * Stack Design only works in WordPress 4.1 or later.
 */
if (version_compare($GLOBALS['wp_version'], '4.1-alpha', '<'))
{
	require get_template_directory() . '/inc/back-compat.php';
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 * we here define stackdesign_setup function to initialize theme.
 */

if (!function_exists('stackdesign_setup')) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * @since Twenty Fifteen 1.0
	 */
	function stackdesign_setup()
	{

		/*
		 * Make theme available for translation.
		 * Translations can be filed at WordPress.org. See: http://wordpress.org
		 * If you're building a theme based on stackdesign, use a find and replace
		 * to change 'stackdesign' to the name of your theme in all the template files
		 */
		// Stack design Loading the theme's translated strings.

		load_theme_textdomain('stackdesign', get_template_directory() . '/languages');


		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');
		add_theme_support('post-thumbnails');
		add_image_size('custom-thumbnail', 700, 432, true);


		// This theme uses wp_nav_menu() For two language.

		register_nav_menus(array(
			'primary' => __('Persian Menu', 'stackdesign'),
			'social'  => __('English Menu', 'stackdesign'),
		));

		/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
		add_theme_support(
			'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		* Enable support for Post Formats.
		*
		* See: https://codex.wordpress.org/Post_Formats
		*/
		add_theme_support(
			'post-formats', array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'status',
				'audio',
				'chat',
			)
		);

		/*
		 * Enable support for custom logo.
		 *
		 * @since Stack Design .1
		 */
		add_theme_support(
			'custom-logo', array(
				'height'      => 100,
				'width'       => 100,
				'flex-height' => true,
				'flex-width'  => true,
				'header-text' => array('site-title', 'site-description'),
			)
		);


	}
endif;// stackdesign_setup
add_action('after_setup_theme', 'stackdesign_setup');

/**
 * Register widget areas.
 *
 * @since Stack Design 0.1
 *
 */
//Sidebar implentation

function custom_sidebar_init()
{
	register_sidebar(array(
		'name'          => 'New Custom Sidebar',
		'id'            => 'custom_sidebar_1',
		'description'   => 'Add widgets here to appear for single posts.',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}

//Topbar implentation
function custom_topbar_init()
{
	register_sidebar(array(
		'name'          => 'Custom Topbar',
		'id'            => 'custom_topbar_1',
		'description'   => 'Add widgets here to appear on the top of posts',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}

//Bottombar 1 implentation
function custom_bottombar1_init()
{
	register_sidebar(array(
		'name'          => 'Custom Bottombar 1',
		'id'            => 'custom_bottombar_1',
		'description'   => 'Add widgets here to appear on the bottom of posts',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}

//Bottombar 2 implentation
function custom_bottombar2_init()
{
	register_sidebar(array(
		'name'          => 'Custom Bottombar 2',
		'id'            => 'custom_bottombar_2',
		'description'   => 'Add widgets here to appear on the bottom of posts',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}

//Bottombar 3 implentation
function custom_bottombar3_init()
{
	register_sidebar(array(
		'name'          => 'Custom Bottombar 3',
		'id'            => 'custom_bottombar_3',
		'description'   => 'Add widgets here to appear on the bottom of posts',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}

//Footer bar 1 implentation
function custom_footer_bar_1_init()
{
	register_sidebar(array(
		'name'          => 'Custom Footer bar 1',
		'id'            => 'custom_footer_bar_1',
		'description'   => 'Add widgets here to appear on the bottom of posts',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}

function custom_footer_social_init()
{
	register_sidebar(array(
		'name'          => 'Custom footer social bar',
		'id'            => 'custom_footer_social_bar',
		'description'   => 'Add widgets here to appear on the bottom of posts',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}

add_action('widgets_init', 'custom_sidebar_init');
add_action('widgets_init', 'custom_topbar_init');
add_action('widgets_init', 'custom_bottombar1_init');
add_action('widgets_init', 'custom_bottombar2_init');
add_action('widgets_init', 'custom_bottombar3_init');
add_action('widgets_init', 'custom_footer_bar_1_init');
add_action('widgets_init', 'custom_footer_social_init');


/**
 * Enqueue scripts and styles.
 *
 * @since Stack Design 0.9
 */
function stackdesign_scripts()
{


	//Register and enqueue  css,bootstrap files and jQuery:

	if (get_locale() == ('fa_IR'))
	{

		wp_register_style('style-imports-rtl', get_template_directory_uri() . '/css/imports-rtl.css');
		wp_enqueue_style('style-imports-rtl');

	}
	else
	{

		wp_register_style('style-imports', get_template_directory_uri() . '/css/imports.css');
		wp_enqueue_style('style-imports');

	}

	global $preview, $link, $flag_preview;
	if ($flag_preview == 1)
	{
		// $link='https://blog.stackprogramer.xyz/wp-content/uploads/2019/08/export_theme_2021.txt';
		$request = "/?my-custom-content=css&preview=1&link=" . $link;
	}
	else
	{
		$request = "/?my-custom-content=css";
	}
	wp_register_style('theme-custom', get_bloginfo('url') . $request, '1.0', 'all');
	wp_enqueue_style('theme-custom');
	wp_register_script('jquery.bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', 'jquery', array(), '1.0.0', true);
	wp_register_script('jquery.min', get_template_directory_uri() . '/js/jquery.min.js', 'jquery');
	wp_enqueue_script('jquery.bootstrap.min');
	wp_enqueue_script('jquery.min');


}

add_action('wp_enqueue_scripts', 'stackdesign_scripts');


/*
 * Pagination defined
 */

if (!function_exists('stackdesign_themes_pagination')) :

	//Pagination function implentation
	function stackdesign_themes_pagination($echo = true)
	{
		global $wp_query;

		$big = 999999999; // need an unlikely integer

		$pages = paginate_links(array(
				'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
				'format'    => '?paged=%#%',
				'current'   => max(1, get_query_var('paged')),
				'total'     => $wp_query->max_num_pages,
				'type'      => 'array',
				'prev_next' => true,
				'prev_text' => __('Previous page', 'stackdesign'),
				'next_text' => __('Next page', 'stackdesign'),
			)
		);

		if (is_array($pages))
		{
			$paged = (get_query_var('paged') == 0) ? 1 : get_query_var('paged');

			$pagination = '<ul class="pagination pagination-lg">';

			foreach ($pages as $page)
			{
				$pagination .= "<li>$page</li>";
			}

			$pagination .= '</ul>';

			if ($echo)
			{
				echo $pagination;
			}
			else
			{
				return $pagination;
			}
		}
	}

endif;

/*
 * Gravatar Alt Fix
 */
function stackdesign_gravatar_alt($text)
{
	$alt  = get_the_author_meta('display_name');
	$text = str_replace('alt=\'\'', 'alt=\'Avatar for ' . $alt . '\' title=\'Gravatar for ' . $alt . '\'', $text);

	return $text;
}


/*
 * Function for adding counter views/hits to wordpress post.
 *  display number of hits posts.
 */

function getPostViews($postID)
{
	$count_key = 'post_views_count';
	$count     = get_post_meta($postID, $count_key, true);
	if ($count == '')
	{
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');

		return "0";
	}

	return $count;
}

// function to count views.
function setPostViews($postID)
{
	$count_key = 'post_views_count';
	$count     = get_post_meta($postID, $count_key, true);
	if ($count == '')
	{
		$count = 0;
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
	}
	else
	{
		$count++;
		update_post_meta($postID, $count_key, $count);
	}
}

// Add it to a column in WP-Admin
function posts_column_views($defaults)
{
	$defaults['post_views'] = __('Views');

	return $defaults;
}

function posts_custom_column_views($column_name, $id)
{
	if ($column_name === 'post_views')
	{
		echo getPostViews(get_the_ID());
	}
}

/*
 * Add google analytic section....enable_google_analytic_setting
 */
$enable_google_analytic_setting = get_theme_mod('enable_google_analytic_setting', 'default');


function ns_google_analytics()
{

	$google_analytic_code_stackdesgin = get_theme_mod('google_analytic_code_stackdesgin', 'default');
	$enable_google_analytic_setting   = get_theme_mod('enable_google_analytic_setting', 'default');

	?>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121810903-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', '<?php  echo $google_analytic_code_stackdesgin;  ?>');
    </script>
	<?php


}

if ($enable_google_analytic_setting)
{
	add_action('wp_head', 'ns_google_analytics', 10);
}


/*
 * function handle contact us form
 */
function my_contact_form_func()
{

	if (
		!empty($_GET['msg_name']) && !empty($_GET['msg_email']) && !empty($_GET['msg_subject']) && !empty($_GET['msg_message'])

	)
	{
		$name    = $_GET['msg_name'];
		$email   = $_GET['msg_email'];
		$subject = $_GET['msg_subject'];
		$message = $_GET['msg_message'];

		$to = 'stackprogramer@gmail.com';

		if (strlen($name) == 0 || strlen($email) == 0 || strlen($subject) == 0 || strlen($message) == 0)
		{
			echo '<div style="text-align: center;background:orange;padding:5px;border-radius:3px;margin:3px 0px;color:#fff;font-size:14px;">لطفا تمامی فیلد ها را پر کنید</div>';
		}
		else
		{
			if (mail($to, $subject, "از طرف : " . $email . "\r\n" . "پیام :" . $message))
			{
				echo '<div style="text-align: center;background:green;padding:5px;border-radius:3px;margin:3px 0px;color:#fff;font-size:14px;">پیام شما با موفقیت ارسال شد</div>';
			}
			else
			{
				echo '<div style="text-align: center;background:red;padding:5px;border-radius:3px;margin:3px 0px;color:#fff;font-size:14px;">مشکلی در ارسال پیش آمد.دوباره تلاش کنید</div>';
			}
		}
	}
	else
	{
		echo "notgggggggggg found";
	}
}

add_action('admin_post_nopriv_my_contact_form', 'my_contact_form_func');
add_action('admin_post_my_contact_form', 'my_contact_form_func');


/*
 * remove some style from comments
 */
function my_remove_recent_comments_style()
{
	global $wp_widget_factory;
	remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}

add_action('widgets_init', 'my_remove_recent_comments_style');

// Adding  actions of functions.
add_filter('get_avatar', 'stackdesign_gravatar_alt');
// Add it to a column in WP-Admin
add_filter('manage_posts_columns', 'posts_column_views');
add_action('manage_posts_custom_column', 'posts_custom_column_views', 5, 2);
// End adding actions of functions.


// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');


/**
 * Adding nawwalker menu to theme.
 *
 * @since stackdesign .11
 */
require_once('inc/wp_bootstrap_navwalker.php');


/**
 * Custom template tags for this theme.
 *
 * @since stackdesign .11
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 *
 * @since Stack Design .11
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * relative url.
 *
 * @since Stack Design 1.6
 */
$url_shortener_setting = get_theme_mod('url_shortener_setting', 'default');
if ($url_shortener_setting == 1)
{
	require_once('inc/relative-url.php');
}



				
						



