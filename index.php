<?php get_header(); ?>
<?php
if (is_active_sidebar('custom_topbar_1'))
{
	echo "<!-- Topbar position -->
                          <section   class='row'>";
	dynamic_sidebar('custom_topbar_1');
	echo "</section><!-- End topbar position -->";
}
?>
<div class="row">
    <div class="col-sm-2 col-md-2 ">
    </div>
    <main class="col-sm-8 col-md-8 blog-main">
		<?php
		if (have_posts()) : while (have_posts()) : the_post();

			get_template_part('content', get_post_format());

		endwhile;
		endif;
		// Previous/next page navigation.
		stackdesign_themes_pagination($echo = true);
		?>
    </main>
    <!-- /.blog-main -->
	<?php //get_sidebar(); ?>
</div>


<div class='row'>
	<?php
	if (is_active_sidebar('custom_bottombar_1'))
	{
		echo "<!--   Bottombar position -->
                                    <section class='col-xm-4 col-sm-4 col-md-4 col-lg-4'>";
		dynamic_sidebar('custom_bottombar_1');
		echo "</section>";
	}
	?>
	<?php
	if (is_active_sidebar('custom_bottombar_2'))
	{
		echo "<!--   Bottombar position -->
                                    <section class='col-xm-4 col-sm-4 col-md-4 col-lg-4'>";
		dynamic_sidebar('custom_bottombar_2');
		echo "</section>";

	}
	?>
	<?php
	if (is_active_sidebar('custom_bottombar_3'))
	{
		echo "<!--   Bottombar position -->
                                    <section class='col-xm-4 col-sm-4 col-md-4 col-lg-4'>";
		dynamic_sidebar('custom_bottombar_3');
		echo "</section> <!-- End  bottombar position -->";

	}
	?>

</div>

<?php get_footer(); ?>
