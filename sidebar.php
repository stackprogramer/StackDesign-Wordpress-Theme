<?php
/**
 * The template for displaying side bar
 */ ?>

<br>
<br>
<aside class="col-sm-3 col-md-3 blog-sidebar">
	<?php if (is_active_sidebar('custom_sidebar_1')) : ?>
        <aside role="complementary">
			<?php dynamic_sidebar('custom_sidebar_1'); ?>
        </aside>
        <!-- #primary-sidebar -->
	<?php endif; ?>
</aside>
<!-- /.blog-sidebar -->