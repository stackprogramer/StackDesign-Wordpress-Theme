</div>
<!-- /.container -->

<!-- Footer -->
<footer class="blog-footer page-footer ">

    <div class="container">


		<?php
		if (is_active_sidebar('custom_footer_social_bar'))
		{

			echo ' <!-- Grid row-->
                            <div class="row py-4 d-flex align-items-center">

                                <!-- Grid column -->
                                <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                                    <h6 class="mb-0">' . _e('Get connected with us on social networks!', 'stackdesign') . '</h6>
                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6 col-lg-7 text-center text-md-right">';
			echo "<!--   Bottombar position -->
                                <section class='col-xm-4 col-sm-4 col-md-4 col-lg-4'>";
			dynamic_sidebar('custom_footer_social_bar');
			echo "</section>";
			echo '    </div>
                                <!-- Grid column -->
                            </div>
                            <!-- Grid row-->';
		}
		?>


    </div>

    <!-- Footer Links -->
    <div class="container text-center text-md-left mt-5">

        <!-- Grid row -->
        <div class="row mt-3 dark-grey-text">

            <!-- Grid column -->
            <div class="col-md-4 col-lg-4 col-xl-4 mb-4">

                <!-- Content -->
                <h6 class="text-uppercase font-weight-bold"><?php echo get_bloginfo('name'); ?></h6>
                <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p><?php echo get_bloginfo('description'); ?></p>
				<?php
				$copyright_footer_text = get_theme_mod('copyright_footer_text', 'default');
				if (isset($copyright_footer_text) && $copyright_footer_text != null)
					echo "<h3>" . $copyright_footer_text . "</h3>";
				else
					echo "<h3>©" . date(' Y') . '  ' . get_bloginfo('name') . "</h3>";
				?>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-4">

                <!-- Links -->
                <h6 class="font-weight-bold"><?php echo __('Categorizes', 'stackdesign'); ?></h6>
                <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
				<?php

				$categories = get_categories();
				foreach ($categories as $cat)
				{
					$category_link = get_category_link($cat->cat_ID);
					echo '<a href="' . esc_url($category_link) . '" title="' . esc_attr($cat->name) . '">{' . $cat->name . '}</a>';
				}
				?>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-4">

                <!-- Links -->
                <h6 class="font-weight-bold"><?php echo __('Tags', 'stackdesign'); ?></h6>
                <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">

                <p><?php

					$tags = get_tags();
					$html = '<div class="post_tags">';
					foreach ($tags as $tag)
					{
						$tag_link = get_tag_link($tag->term_id);

						$html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
						$html .= "#{$tag->name}</a>";
					}
					$html .= '</div>';
					echo $html;


					?></p>

            </div>
            <!-- Grid column -->


        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright">
        <hr>
        قالب بوت استرپ فارسی اثر <a href="https://stackprogramer.xyz/">استک پروگرمر</a>
        <br>
        <a href="#"><?php echo _e('Back to top', 'stackdesign') ?></a>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->

<?php wp_footer(); ?>
</body>
</html>